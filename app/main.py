from flask import Flask, jsonify
import mlconjug

app = Flask(__name__)

@app.route('/conjug/<string:verb>')
def conjug(verb):
    conjugator = mlconjug.Conjugator('fr')
    result = conjugator.conjugate(verb)
    return jsonify(result.conjug_info)

@app.route('/hello')
def hello():
    return 'Hello World!'

#if __name__ == "__main__":
#    app.run(host='0.0.0.0', debug=True, port=80)
