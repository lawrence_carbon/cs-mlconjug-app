FROM tiangolo/uwsgi-nginx-flask:python3.6

# copy over our requirements.txt file
COPY requirements.txt /tmp/

# upgrade pip and install required python packages
#RUN pip install -U pip
#RUN pip install -r /tmp/requirements.txt

RUN apt-get update && apt-get install -y git && mkdir install-mlconjug && cd install-mlconjug \
    && git clone git://github.com/SekouD/mlconjug \
    && cd mlconjug && python setup.py install

COPY ./app /app
